
declare
didJoueur char(2):='&vidJoueur';
didReserv char(3);
ddateReserv date;
dmessage varchar2(200);
inexistant exception;
probleme exception;
dcpt number;
dcptReserv number;
begin
	
	dmessage:='joueur inconnu';
	select count(*) into dcpt
		from joueur
		where idJoueur=didJoueur;
	if dcpt=0
	then raise inexistant;
	else select count(*) into dcptReserv
		from faire F, reservation R
		where F.idReserv=R.idReserv
			and F.idJoueur=didJoueur 
			and R.dateReserv >= sysdate;
		dmessage:='Impossible, il existe des réservations faites par ce joueur pour aujourd''hui ou dans le futur';
		if dcptReserv>0
		then raise probleme;
		else select count(*) into dcptReserv
			from faire F, reservation R
			where F.idReserv=R.idReserv
			and F.idJoueur=didJoueur 
			and R.dateReserv < sysdate;
			if dcptReserv>0
			then delete from faire
				where idJoueur=didJoueur;
				insert into tresultat values(to_char(dcptReserv)||' action(s) de réservations faites dans le passé par ce joueur ont été supprimées');
			end if;
		delete from joueur where idJoueur=didJoueur;	
		insert into tresultat values('joueur supprimé');
		commit;
		end if;	
	end if;
	exception
	when inexistant then insert into tresultat values (dmessage);
	when probleme then insert into tresultat values (dmessage);
end;




