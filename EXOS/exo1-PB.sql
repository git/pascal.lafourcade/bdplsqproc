drop table tresultat purge;
create table tresultat(ligne varchar2(200));

variable vidReserv char(3)
prompt id Reservation?
accept vidReserv
variable vdateReserv char(9)
prompt date reservation?
accept vdateReserv
variable vnbJoueurs number
prompt nombre joueurs?
accept vnbJoueurs
variable vidTerrain char(5)
prompt id Terrain?
accept vidTerrain
variable vnumCreneau char(2)
prompt num Creneau?
accept vnumCreneau

declare
didReserv char(3):='&vidReserv';
ddateReserv date:=to_date('&vdateReserv', 'dd-mon-yy');
dnbJoueurs number:=&vnbJoueurs; 
didTerrain char(5):='&vidTerrain';
dnumCreneau char(2):='&vnumCreneau';

dmessage varchar2(200);
probleme exception;
dcpt number;

begin
	
	dmessage:='ERREUR: id réservation déjà attribué';
	select count(*) into dcpt
		from reservation
		where idReserv=didReserv;
	if dcpt=1
	then insert into tresultat values (dmessage);
	else dmessage:='La date de réservation doit être postérieure à la date du jour';
		if ddateReserv < sysdate 
		then  raise probleme;
		else dmessage:='l''identifiant du terrain est inconnu';
			select count(*) into dcpt
			from terrain
			where idTerrain=didTerrain;
			id dcpt=0 then raise probleme;
			else 
			dmessage:='l''identifiant du créneau est inconnu';
			select count(*) into dcpt
			from creneau
			where numCreneau=dnumCreneau;
			id dcpt=0 then raise probleme;
			else			
			insert into reservation values(didReserv, ddateReserv, dnbJoueurs, didTerrain, dnumCreneau);
			commit;
			insert into tresultat values ('La réservation a été enregistrée :');
			end if;
		end if;
	end if;
	exception
	when probleme then insert into tresultat values (dmessage);
end;
.
/
select * from tresultat;



