
DECLARE

   didReservation char(3);
   ddateReserv date;
   dnom varchar2(20);
   dancidReservation char(3);
   
   dmessage varchar2(200);
   inexistant exception;
   dcpt number:=0;
   dnb number:=0;

   CURSOR cur IS SELECT idReservation, dateReserv, nom
      FROM CONTENIR con, RESERVATION res, FAIRE fai, JOUEUR jou
      WHERE con.idReserv = res.idReserv AND res.idReserv = fai.idReserv AND fai.idJoueur = jou.idJoueur
      AND dateReserv >= SYSDATE AND con.idEquipement = '&videquip'
      ORDER BY 1,2,3;
BEGIN
      dmessage:='id équipement inexistant';
      SELECT COUNT(*) INTO dcpt
      FROM EQUIPEMENT
      WHERE idEquipement='&videquip';

   IF dcpt=0 THEN 
      RAISE inexistant;
   ELSE 
      OPEN cur;
         FETCH cur INTO didReservation, ddateReserv, dnom;
	    WHILE cur%FOUND
	    LOOP
	       INSERT INTO tresultat VALUES ('idReservation:' || didReservation || ' date:' || TO_CHAR(ddateReserv, 'dd/mm/yy'));
	       dancidReservation:=didReservation;
	    
	       WHILE dancidReservation=didReservation AND cur%FOUND
	       LOOP
	          INSERT INTO tresultat VALUES (dnom);
	          dnb := dnb+1;

	          FETCH cur INTO didReservation, ddateReserv, dnom;
	       END LOOP;
	    END LOOP;

	    CLOSE cur;

	    INSERT INTO tresultat VALUES ('');
	    INSERT INTO tresultat VALUES ('Nombre de joueurs à prévenir : ' || TO_CHAR(dnb));
	END IF;

EXCEPTION
   WHEN inexistant THEN
      INSERT INTO tresultat VALUES (dmessage);
END;



