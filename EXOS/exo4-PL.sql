drop table tresultat purge;
create table tresultat(ligne varchar2(200));
declare
dnumCreneau varchar2(10);
didReservation char(3);
dhdebut number;
dbenefT number;
dbenefE number;
dbenef number;
dnbjoueur number;

cursor CUR is select F.numCreneau, sum(F.tarifHoraire*S.duree) + sum(E.tarif*C.nombre), count(FR.idJoueur)
from reservation R, creneau CR, facturer F, sport S , faire FR, contenir C, equipement E, terrain T
WHERE S.nomSport=T.nomSport
and T.idTerrain=R.idTerrain
and R.numCreneau=CR.numCreneau
and CR.numCreneau=F.numCreneau
and S.nomSport= F.nomSport 
and FR.idReserv=R.idReserv
and R.idReserv=C.idReserv
and E.idEquipement=C.idEquipement
group by F.numCreneau
order by 2;


begin
open CUR;
fetch CUR into  dnumCreneau, dbenef, dnbJoueur;
while CUR%FOUND
loop 
select heureDebut into dhdebut from creneau CR where CR.numCreneau=dnumCreneau;
insert into tresultat values('NumCreneau:'||dnumCreneau||'  Heure debut:'||dhdebut ||' Benefice:'||dbenef||'  Nb joueurs:'||dnbjoueur);
fetch CUR into dnumCreneau, dbenef, dnbJoueur;
end loop;
close CUR; 
end;
.
/
select * from tresultat;