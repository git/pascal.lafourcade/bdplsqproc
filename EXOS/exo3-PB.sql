
declare
didTerrain char(3):='T03';
didReserv char(3);
ddateReserv date;
dnom varchar2(20);
dancidReservation char(3);
dmessage varchar2(200);
inexistant exception;
dcpt number;
dnb number:=0;
cursor cur is select idReserv, dateReserv, nom
			from reservation R, faire F, joueur J
			where R.idReservation = F.idReservation
			and J.idJoueur=F.idJoueur
			and R.idTerrain=didTerrain
			order by 2,3;
begin
	dmessage:='id Terrain inexistant';
	select count(*) into dcpt
		from terrain
		where idTerrain=didTerrain;
	if dcpt=0
	then raise inexistant;
	else 
		open cur;
		fetch cur into didReserv, ddateReserv, dnom;
		while cur%FOUND
		loop
			insert into tresultat values('idReservation:'||didReservation||' date:'||to_char(ddateReserv, 'dd/mm/yy'));
			dancidReservation:=didReservation;
			while dancidReservation=didReservation and cur%FOUND
			loop
			insert into tresultat values (dnom);
			dnb:=dnb+1;
			fetch cur into didReserv, ddateReserv, dnom;
			end loop;
		end loop;
		close cur;
		insert into tresultat values ('Nombre de joueurs à prévenir : '||to_char(dnb));
	end if;
	exception
	when inexistant then insert into tresultat values (dmessage);
end;



