DECLARE

didReserv char(5);
   ddateReserv date;
   dcout number;
   
   CURSOR cur IS SELECT idReserv, dateReserv, SUM(nombre*tarif)
		    FROM RESERVATION res, CONTENIR con, EQUIPEMENT equ
		    WHERE con.idReserv = res.idReserv AND con.idEquipement = equi.idEquipement 
		    group by idReserv, dateReserv
		    order by 2 DESC;
BEGIN
	
   OPEN cur;
   FETCH cur INTO didReserv, ddateReserv,  dcout;
   
   WHILE cur%FOUND
   LOOP
      INSERT INTO tresultat VALUES ('Id reservation : ' || didReserv || ' date : ' || TO_CHAR(ddateReserv,'dd/mm/yy') || ' Cout des uquipements : ' || TO_CHAR(dcout) || 'euros');
      INSERT INTO tresultat VALUES ('');
      FETCH cur INTO didReserv, ddateReserv,  dcout;
   END LOOP;
   
   CLOSE cur;

END;
