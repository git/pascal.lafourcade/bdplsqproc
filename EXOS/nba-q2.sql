@nba-setup.sql;

DROP TABLE tligne ;
CREATE TABLE tligne (ligne varchar2(100)) ;

set echo off;
set verify off;
set feedback off;

DECLARE
dnom varchar2(20);
dprenom varchar2(20);
ddate date;
dnomequipe varchar2(20);

CURSOR c IS SELECT j.Nom, j.Prenom, j.Date_de_naissance, e.Nom  FROM JOUEUR j, APPARTIENT a, EQUIPE e 
WHERE a.id_joueur = j.id_joueur and e.id_equipe =  a.id_equipe and a.Date_fin is NULL
Order by Date_de_naissance;


BEGIN
OPEN c;

INSERT INTO tligne VALUES ('Nom  Prenom Date de naissance Nom Equipe Actuelle');
FETCH c INTO dnom, dprenom, ddate, dnomequipe;

WHILE c%found
LOOP  
  INSERT INTO tligne VALUES (dnom||' '||dprenom||' '||to_char(ddate)||' '||dnomequipe); 
  FETCH c INTO dnom, dprenom, ddate, dnomequipe;
END LOOP;

CLOSE C;
END;
.
/

SELECT * FROM tligne ;
set verify on;
set feedback on;
set echo on;
