
declare
didEquip char(2):='&vidEquip';
didReserv char(3);
dmessage varchar2(200);
inexistant exception;
dcpt number;
dcptReserv number;
cursor R is select idReservation from reservation R, contenir C where R.idReservation=C.idReservation 
and dateReserv>='01-mar-21' and dateReserv<='31-mar-21' and C.idEquipement=didEquip;
begin
	
	dmessage:='l''identifiant de l''equipement est inconnu';
	select count(*) into dcpt
		from equipement
		where idEquipement=didEquip;
	if dcpt=0
	then raise inexistant;
		
	else  dcptReserv:=0;
			open R;
			fetch R into didReservation;
			while R%found
			loop
				delete from contenir 
				where idReservation=didReservation;
				delete from faire
				where idReservation = didReservation;
				delete from reservation where idReservation= didReservation;
				dcptReserv:=dcptReserv+1;
				fetch R into didReservation;
			end loop;
			close R;
		insert into tresultat values(to_char(dcptReserv)||' réservations ont été supprimées');
		commit;
	end if;
	exception
	when inexistant then insert into tresultat values (dmessage);
end;
