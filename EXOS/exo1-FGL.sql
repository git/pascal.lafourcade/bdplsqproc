drop table tresultat purge;
create table tresultat(ligne varchar2(200));

variable vidEquipement char(3)
prompt id de l equipement
accept vidEquipement 

variable vtarif number
prompt le tarif de l equipement ?
accept vtarif

variable vlibelle VARCHAR2(20)
prompt libellé équipement ?
accept vlibelle 

DECLARE
	dmessage varchar2(200);
	probleme exception;
	dcpt number;

BEGIN
	
	dmessage := 'ERREUR: id equipement deja attribue';
	SELECT COUNT(*) INTO dcpt
		FROM EQUIPEMENT
		WHERE idEquipement = '&vidEquipement';
	IF dcpt=1 THEN 
	   INSERT INTO tresultat VALUES (dmessage);
	ELSE 
	   dmessage := 'ERREUR: Le tarif doit etre superieure a 0';
	   IF '&vtarif' <= 0 then  
              raise probleme;
	   ELSE
	      dmessage := 'ERREUR: le libelle de l equipement doit etre unique';
	      SELECT count(*) INTO dcpt
	      FROM EQUIPEMENT
	      WHERE libelle = '&vlibelle';
	      
	      IF dcpt>0 then 
	         raise probleme;
	      ELSE 
		 INSERT INTO EQUIPEMENT VALUES ('&vidEquipement', '&vtarif', '&vlibelle');
		 COMMIT;
		 INSERT INTO tresultat VALUES ('L equipement a ete enregistre :' || '&vlibelle');
	      END IF;
	END IF;

EXCEPTION
	WHEN probleme then 
		INSERT INTO tresultat VALUES (dmessage);
END;
.
/

SELECT * FROM tresultat;

