DROP TABLE ETUDIANT2020;
CREATE TABLE ETUDIANT2020(nom varchar2(100), age number);

INSERT INTO ETUDIANT2020 VALUES('Alice', 12);
INSERT INTO ETUDIANT2020 VALUES('Bob', 15);
INSERT INTO ETUDIANT2020 VALUES('Charlie', 18);
INSERT INTO ETUDIANT2020 VALUES('Dave', 21);
INSERT INTO ETUDIANT2020 VALUES('Eve', 11);

DROP TABLE tligne;
CREATE TABLE tligne(DESCRIPTION VARCHAR2(300));

SET echo off;
SET verify off;
SET feedback off;

DECLARE
dnom VARCHAR2(100);

CURSOR C IS SELECT nom from ETUDIANT2020 WHERE (age >= 18);

BEGIN
OPEN C;
FETCH C into dnom;
WHILE C%FOUND
 LOOP
 INSERT INTO tligne VALUES(dnom);
 FETCH C into dnom;
END LOOP;
CLOSE C;
END;
.
/
SET echo on;
SET verify on;
SET feedback on;

SELECT * FROM TLIGNE;
