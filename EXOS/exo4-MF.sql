declare
dnomSport varchar(10);
didReserv char(5);
dheureDebut number;
dduree number;
dtarifHoraire number;
dcout number;

cursor CUR is select idReserv,  heureDebut,  duree, tarifHoraire, duree*tarifHoraire
			from creneau C, reservation R, terrain T, sport S, facturer F
			where S.nomSport = F.nomsport
			and S.nomSport=T.nomSport
			and T.idTerrain=R.idTerrain
			and R.numCreneau=C.numCreneau
			and F.numCreneau=C.numCreneau
			order by 5 desc;
begin
	
		open CUR;
		fetch CUR into didReserv,  dheureDebut,  dduree, dtarifHoraire, dcout;
		while CUR%FOUND
		loop
			insert into tresultat values('Id reservation:'||ddidReserv||' heure de début:'||to_char(dheureDebut)||' durée:'||to_char(dduree)||' tarif horaire:'||
					to_char(dtarifHoraire)||' cout du terrain:'||to_char(dcout)||'euros');
			fetch CUR into didReserv,  dheureDebut,  dduree, dtarifHoraire, dcout;
			end loop;
		end loop;
		close CUR;
end;


