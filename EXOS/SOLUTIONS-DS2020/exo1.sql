drop table tresultat purge;
create table tresultat(ligne varchar2(200));
variable vnbRef number
prompt nombre de référence?
accept vnbRef
declare
dnbRef number:='&vnbRef';
didRep char(5);
dmessage varchar2(200);
dnbTot number;
inex exception;
cursor CUR is select I.idRep, sum(nbPlacesRes)
			from inscription I, representation R
			where I.idRep=R.idRep
			and dateRep>sysdate
			group by I.idRep	
			having sum(nbPlacesRes) > dnbRef	
			order by 2 desc;
begin
	dmessage:='nombre de reference trop eleve';
	insert into tresultat values('Id rep     Nb total');
		open CUR;
		fetch CUR into didRep, dnbTot;
		
		if CUR%notFound then raise inex;end if;
		while CUR%FOUND
		loop
			insert into tresultat values (didRep||'     '||dnbTot);
			fetch CUR into didRep, dnbTot;
		end loop;
		close CUR;
	exception
	when inex then insert into tresultat values (dmessage);
end;
.
/
select * from tresultat;




