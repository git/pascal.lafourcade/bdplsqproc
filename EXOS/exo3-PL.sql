drop table tresultat purge;
create table tresultat(ligne varchar2(200));
variable vnumcreneau varchar2(10)
prompt num creneau ?
accept vnumcreneau

declare
dnumCreneau varchar2(10):='&vnumcreneau';
didReserv char(3);
dnom varchar2(20);
ddateReserv date;
dnumCreneau char(2);
dmessage varchar2(200);
inexistant exception;
probleme exception;
dcpt number;
dnb number:=0;
cursor CUR is select idReserv, dateReserv
			from reservation R
			where  numCreneau=dnumCreneau
			order by 2;

cursor CURJOUEUR is select Nom
			from  Faire F, Joueur J
			where F.idReserv=didReserv
			and J.idjoueur=F.idjoueur;
			order by 2;

begin
	dmessage:='num du creneau inexistant';
	select count(*) into dcpt
		from creneau
		where numCreneau=dnumCreneau;
	if dcpt=0
	then raise inexistant;
	else 
		open CUR;
		fetch cur into  didReserv, ddateReserv;
		
		while cur%FOUND
		loop
			insert into tresultat values('Id reservation:'||didReserv||'   Date :'||to_char(ddateReserv));
			open CURJ;
			fetch curJ into  dnom;
			while  CURJ%FOUND
			loop
			insert into tresultat values (dnom);
			dnb:=dnb+1;
			fetch curJ into dnom;
			end loop;
			close CURJ;
		end loop;
		close CUR;
		insert into tresultat values ('Nombre de joueurs a prevenir : '||to_char(dnb));
	end if;
	exception
	when inexistant then insert into tresultat values (dmessage);
	when probleme then insert into tresultat values (dmessage);
end;
.
/
select * from tresultat;



