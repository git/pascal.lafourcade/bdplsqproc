@2020nba-setup.sql;
DROP TABLE tligne ;
CREATE TABLE tligne (ligne varchar2(100));

set echo off;
set verify off;
set feedback off;

variable vidjoueur varchar2(10)
prompt Entrer l idjoueur
accept vidjoueur

variable vnom varchar2(300)
prompt Entrer le nom du joueur
accept vnom

variable vprenom varchar2(200)
prompt Entrer le prenom du joueur
accept vprenom

variable vdate varchar2(20)
prompt Entrer sa datee de naissance
accept vdate

variable vposte varchar2(2)
prompt Entrer le poste
accept vposte

variable vtaille NUMBER
prompt Entrer sa taille
accept vtaille

DECLARE
dnbid number;
EXISTEDEJA EXCEPTION;

BEGIN
SELECT count(*) INTO dnBid FROM JOUEUR WHERE id_joueur ='&vidjoueur';
IF dnbid > 0 THEN RAISE EXISTEDEJA;
ELSE
INSERT INTO tligne VALUES ('&vidjoueur'||'&vnom'||'&vprenom'||to_date('&vdate','DD/MM/YYYY')||'&vtaille'||'&vposte');
END IF;

EXCEPTION
WHEN EXISTEDEJA THEN
INSERT INTO tligne VALUES ('ID joueur existe');

END;
./

set echo on;
set verify on;
set feedback on;

SELECT * FROM TLIGNE;
