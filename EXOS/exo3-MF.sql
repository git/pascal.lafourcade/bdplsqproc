declare
dnomSport varchar2(10):='&vnomsport';
didReserv char(3);
ddateReserv date;
dheureDebut number;
dancienheureDebut char(3);
dmessage varchar2(200);
inexistant exception;
dcpt number;
dnb number:=0;
cursor cur is select heureDebut, idReserv, dateReserv
			from creneau C, reservation R, terrain T, sport S
			where S.nomSport = dnomSport
			and S.nomSport=T.nomSport
			and T.idTerrain=R.idTerrain
			and R.numCreneau=C.numCreneau
			order by 1,3;
begin
	dmessage:='nom du sport inexistant';
	select count(*) into dcpt
		from sport
		where nomSport=dnomSport;
	if dcpt=0
	then raise inexistant;
	else 
		open cur;
		fetch cur into dheureDebut, didReserv, ddateReserv;
		while cur%FOUND
		loop
			insert into tresultat values('heure de début:'||to_char(dheureDebut));
			dancienheureDebut:=dheureDebut;
			while dancienheureDebut=dheureDebut and cur%FOUND
			loop
			insert into tresultat values (didReserv||'    date:'||ddateReserv);
			dnb:=dnb+1;
			fetch cur into dheureDebut, didReserv, ddateReserv;
			end loop;
		end loop;
		close cur;
		insert into tresultat values ('Nombre de réservations total pour ce sport : '||to_char(dnb));
	end if;
	exception
	when inexistant then insert into tresultat values (dmessage);
end;




