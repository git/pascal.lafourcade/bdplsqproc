DECLARE

   dmessage varchar2(200);
   inexistant exception;
   dcpt number;
   dcptReserv number;

BEGIN
	
   dmessage := 'l''identifiant de l''equipement est inconnu';
   SELECT COUNT(*) INTO dcpt
      FROM equipement
      WHERE idEquipement = '&videquip';

   IF dcpt=0 THEN 
      RAISE inexistant;
   ELSE
      SELECT COUNT(*) INTO dcptReserv FROM CONTENIR con, RESERVATION res
	 WHERE con.idReserv = res.idReserv
	 AND dateReserv >= SYSDATE AND con.idEquipement = '&videquip';
         
      dmessage := 'Des réservations en cours ou à venir utilisent cet équipement';

      IF dcptReserv > 0 THEN
         RAISE resaExiste;
      ELSE
         
	 SELECT COUNT(*) INTO dcptReserv FROM CONTENIR con, RESERVATION res
	 WHERE con.idReserv = res.idReserv AND dateReserv < SYSDATE
	  AND con.idEquipement = '&videquip';

	 DELETE FROM CONTENIR WHERE idReserv IN (
	    SELECT con.idReserv INTO dcptReserv FROM CONTENIR con, RESERVATION res
	       WHERE con.idReserv = res.idReserv AND dateReserv < SYSDATE
	       AND con.idEquipement = '&videquip') AND idEquipement = '&videquip';
	 INSERT INTO tresultat VALUES (TO_CHAR(dcptReserv) || ' contenu de réservations passées ont été supprimées');

	 DELETE FROM EQUIPEMENT WHERE idEquipement = '&videquip';
	 INSERT INTO tresultat VALUES ('Equipement supprimé : ' || '&videquip');

      END IF;
   END IF;

EXCEPTION
   WHEN inexistant THEN 
      INSERT INTO tresultat VALUES (dmessage);
   WHEN resaExiste THEN
      INSERT INTO tresultat VALUES (dmessage);

END;
