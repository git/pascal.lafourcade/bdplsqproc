@2020nba-setup.sql;

DROP TABLE tligne ;
CREATE TABLE tligne (ligne varchar2(300)) ;

set echo off;
set verify off;
set feedback off;

variable vidjoueur char(4)
prompt Entrer la reference du joueur :
accept vidjoueur

DECLARE
dmaxpoint number;
dnbjoueur number;
didjoueur char(4);

BEGIN

SELECT count(Nom) INTO dnbjoueur FROM JOUEUR
WHERE id_joueur ='&vidjoueur';

if dnbjoueur != 0 then

SELECT max(points) INTO dmaxpoint FROM JOUE
WHERE id_joueur ='&vidjoueur';

INSERT INTO tligne VALUES ('Le maximum de point du joueur '||'&vidjoueur'||' est '||to_char(dmaxpoint));
ELSE
INSERT INTO tligne VALUES('AVEC COUNT : Joueur inconnu'); 

SELECT id_joueur INTO didjoueur FROM JOUE
WHERE id_joueur ='&vidjoueur';
END if;


EXCEPTION 
when no_data_found then
INSERT INTO tligne VALUES('AVEC EXCEPTION : Joueur n a pas fait de match'); 
when too_many_rows then
INSERT INTO tligne VALUES('AVEC EXCEPTION : TOO MANY ROWS'); 
end ;
.
/

SELECT * FROM tligne ;
set verify on;
set feedback on;
set echo on;
