drop table tresultat purge;
create table tresultat(ligne varchar2(200));

variable vnomsport varchar2(10)
prompt nom du sport?
accept vnomsport
variable vnbMinJoueurs number
prompt nombre minimum?
accept vnbMinJoueurs
variable vnbMaxJoueurs number
prompt nombre maximum?
accept vnbMaxJoueurs
variable vduree number
prompt duree?
accept vduree

declare
dnomSport varchar2(10):='&vnomsport';
dnbMinJoueurs number:='&vnbMinJoueurs';
dnbMaxJoueurs number:='&vnbMaxJoueurs';
dduree number:='&vduree';
dmessage varchar2(200);
probleme exception;
dcpt number;

begin
	
	dmessage:='ERREUR: nom du sport déjà attribué';
	select count(*) into dcpt
		from sport
		where nomSport=dnomSport;
	if dcpt=1
	then insert into tresultat values (dmessage);
	else dmessage:='Le nombre maximum de joueurs doit être supérieur au nombre minimum';
		if dnbMaxJoueurs < dnbMinJoueurs 
		then  raise probleme;
		else dmessage:='La durée est de 1h ou 2h';
			if dduree =1 or dduree =2
			then 
			insert into sport values(dnomSport, dnbMinJoueurs, dnbMaxJoueurs, dduree);
			commit;
			insert into tresultat values ('Le sport a été enregistré :'|| dnomSport);
			else raise probleme;
			end if;
		end if;
	end if;
	exception
	when probleme then insert into tresultat values (dmessage);
end;
.
/
select * from tresultat;



