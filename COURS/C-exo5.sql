set echo off
set verify off
set feed off

CREATE TABLE tLigne(
	designation VARCHAR2(500)
);

variable vRefProduit CHAR(5)
variable vRefFournisseur CHAR(5);
variable vPrixFournisseur NUMBER;

prompt Tappez la référence du nouveau produit:
accept vRefProduit
prompt Tappez la référence du fournisseur du nouveau produit:
accept vRefFournisseur
prompt Tappez le prix du produit:
accept vPrixFournisseur

DECLARE
dCount NUMBER;
NO_PRODUIT EXCEPTION;
NO_FOURNISSEUR EXCEPTION;
PRODUIT_FOURNISSEUR_ALREADY_EXIST EXCEPTION;


BEGIN

SELECT COUNT(*) INTO dCount FROM tProduit WHERE noProduit = '&vRefProduit';
IF dCount = 0 THEN
  RAISE NO_PRODUIT;


SELECT COUNT(*) INTO dCount FROM tFournisseur WHERE ref = '&vRefFournisseur';
IF dCount = 0 THEN
  RAISE NO_FOURNISSEUR;

SELECT COUNT(*) INTO dCount FROM tProduitFourn WHERE noProduit = '&vRefProduit' AND ref = '&vRefFournisseur';
IF dCount != 0 THEN
  RAISE PRODUIT_FOURNISSEUR_ALREADY_EXIST;

INSERT INTO tProduitFourn VALUES('&vRefProduit', '&vRefFournisseur', '&vPrixFournisseur', NULL, NULL);
INSERT INTO tLigne VALUES('La liaison entre le produit et le fournisseur a ete ajoute');

EXCEPTION
when NO_PRODUIT then
  INSERT INTO tLigne VALUES('La liaison ne peut exister car le produit renseigne n existe pas');
when NO_FOURNISSEUR then
  INSERT INTO tLigne VALUES('La liaison ne peut exister car le fournisseur renseigne n existe pas');
when PRODUIT_FOURNISSEUR_ALREADY_EXIST then
  INSERT INTO tLigne VALUES('La liaison existe deja, et n a donc pas ete ajoute une seconde fois.')

END;
.
/

set verify on
set echo on
set feed on

SELECT * FROM tLigne;
