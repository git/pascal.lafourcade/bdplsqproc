CREATE TABLE tLigne(
    designation VARCHAR2(500)
);

set echo off
set verify off
set feed off

variable vnoP CHAR(5)
variable vql NUMBER
PROMPT Entrer le num du prod :
ACCEPT vnoP
PROMPT Entrer la quantitée livré :
ACCEPT vql

DECLARE
dCo NUMBER;

BEGIN

SELECT stock INTO dCo
FROM tProduit
WHERE noprod = '&vnoP';
dCo:= dCo + &vql;
UPDATE tProduit SET stock = dCo WHERE noprod = '&vnoP';

EXCEPTION
when no_data_found then
insert into Tligne values('référence inconnue');
end;
.
/

set echo on
set verify on
set feed on
