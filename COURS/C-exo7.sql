
set echo off
set verify off
set feed off

CREATE TABLE tLigne(
	designation VARCHAR2(200)
);

variable vNumProduit CHAR(5);
prompt Choisissez un numero de produit
accept vNumProduit

DECLARE
NO_PRODUCTS_SELECTED EXCEPTION;

dCountProduit NUMBER;

BEGIN
SELECT COUNT(*) INTO dCountProduit FROM tProduit WHERE noProduit = '&vNumProduit';
IF dCountProduit = 0 THEN
  RAISE NO_PRODUCTS_SELECTED;

EXCEPTION
when NO_PRODUCTS_SELECTED then
  INSERT INTO tLigne VALUES('Le produit n existe pas');
END;
.
/

SELECT * FROM tLigne;

set echo on
set verify on
set feed on
