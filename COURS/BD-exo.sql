-- ALTER TABLE tfournisseur drop constraint fk_noproduit;
-- ALTER TABLE tfournisseur drop constraint fk_reffour;
-- ALTER TABLE tproduit drop constraint fk_codeRayon;

DROP TABLE tProduitfourn CASCADE CONSTRAINTS;
DROP TABLE tfournisseur CASCADE CONSTRAINTS;
DROP TABLE tproduit CASCADE CONSTRAINTS;
DROP TABLE tRayon CASCADE CONSTRAINTS;
DROP TABLE TClient CASCADE CONSTRAINTS;


CREATE TABLE tRayon(
codeRayon CHAR(4) PRIMARY KEY,
nomRayon VARCHAR2(30),
etage NUMBER(1)
);


CREATE TABLE tProduit(
noProduit CHAR(4) CONSTRAINT pk_tProduit PRIMARY KEY,
designation VARCHAR2(30),
stock NUMBER(4) DEFAULT 0,
prix NUMBER(10,2),
codeRayon CHAR(4) CONSTRAINT fk_codeRayon REFERENCES tRayon
);

CREATE TABLE tClient(
noClient CHAR(4) PRIMARY KEY,
nom VARCHAR2(30),
prenom VARCHAR2(30),
UNIQUE(nom, prenom)
);

CREATE TABLE tfournisseur(
reffourn CHAR(4) PRIMARY KEY,
nom VARCHAR2(200),
qteA1 number,
dpt Number
);


CREATE TABLE tProduitfourn(
noproduit CHAR(4) CONSTRAINT fk_noproduit REFERENCES tproduit,
reffourn CHAR(4) CONSTRAINT fk_reffour REFERENCES tfournisseur,
prixf number(10,2),
qteA1 number,
qte Number,
PRIMARY KEY (noproduit,reffourn)
);


INSERT INTO trayon VALUES (89,'blue',2);
INSERT INTO trayon VALUES (99,'blue',3);

INSERT INTO tProduit VALUES('B004','Imprimante jet 185',NULL, NULL, 89);
INSERT INTO tProduit(noProduit,designation,stock,prix,codeRayon) VALUES('B005','Jet 185',3,20,99);
