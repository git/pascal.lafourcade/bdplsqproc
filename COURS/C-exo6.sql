ET echo OFF
SET verify OFF
SET feed OFF

DROP TABLE tligne;
CREATE TABLE Tligne(ligne VARCHAR2(200));

variable Vref CHAR(5)
PROMPT Donner un numero de fournisseur:
ACCEPT Vref

declare
DnoProduit char(5);

BEGIN

SELECT noProduit INTO DnoProduit
FROM tProduitFourn
WHERE ref = '&Vref';



INSERT INTO Tligne
VALUES('Le fournisseur'||' &Vref '||' a un produit :'|| DnoProduit);


exception
when no_data_found then
INSERT INTO Tligne
VALUES('Le fournisseur n a pas de produit');

WHEN too_many_rows then
INSERT INTO Tligne
VALUES('Le fournisseur a plusieurs produits');
END;
.
/





SELECT * FROM Tligne;

SET echo ON
SET verify ON
SET feed ON
