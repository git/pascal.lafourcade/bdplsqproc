SET echo OFF
SET verify OFF
SET feed OFF

DROP TABLE tligne;
CREATE TABLE Tligne(ligne VARCHAR2(200));

DECLARE
DnoProduit tProduit.noProduit%TYPE;
Ddesignation tProduit.designation%TYPE;
Dstock tProduit.stock%TYPE;
Dclassement NUMBER;
CURSOR r IS SELECT noProduit,designation,stock FROM tProduit ORDER BY stock;

BEGIN
Dclassement := 1;
INSERT INTO Tligne VALUES('noProduit designation stock classement');
OPEN r;
FETCH r INTO DnoProduit,Ddesignation,Dstock;
WHILE r%FOUND
LOOP
	INSERT INTO Tligne VALUES(DnoProduit||' '||Ddesignation||' '||Dstock||' '||Dclassement);
	Dclassement := Dclassement + 1;
	FETCH r INTO DnoProduit,Ddesignation,Dstock;
END LOOP;
CLOSE r;
END;
.
/
