
set echo off
set verify off
set feed off

CREATE TABLE tLigne(
	designation VARCHAR2(200)
);

DECLARE
dStockNull NUMBER := 0;
bin NUMBER;
dNbProduit NUMBER := 0;
CURSOR r IS SELECT stock FROM tProduit;

BEGIN
OPEN r;
FETCH r INTO bin;

WHILE r%FOUND
LOOP
  IF bin = 0 then
    dStockNull := dStockNull + 1;
  END IF;
  dNbProduit := dNbProduit + 1;
  FETCH r INTO bin;
END LOOP;
CLOSE r;

-- Solution sans curseur
-- SELECT COUNT(*) INTO dStockNull FROM tProduit WHERE stock = 0;
-- SELECT COUNT(*) INTO dNbProduit FROM tProduit;

dStockNull := (dStockNull * 100) / dNbProduit;
INSERT INTO tLigne VALUES('Il y a '||dNbProduit||'% de produits avec un stock vide.');

END;
.
/

SELECT * FROM tLigne;
