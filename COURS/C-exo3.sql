DROP TABLE TLigne;
CREATE TABLE TLigne(designation VARCHAR2(500));


variable vnoproduit char(4)

PROMPT Saisir un numero de produit

ACCEPT vnoproduit

DECLARE

dnbfournisseur number;

dnbproduit number;

BEGIN

SELECT COUNT(*) INTO dnbfournisseur FROM TPRODUITFOURN WHERE noproduit = '\&vnoproduit';

IF dnbfournisseur = 0 THEN

  SELECT COUNT(*) INTO dnbproduit FROM TPRODUIT WHERE noproduit = '\&vnoproduit';

  END IF;
  
  IF dnbproduit  = 0 THEN INSERT INTO Tligne VALUES('Pas de produit'||'\&vnoproduit');

ELSE

INSERT INTO Tligne VALUES('Le nombre de fournisseur du produit'
||'\&vnoproduit'||' est '|| dnbfournisseur);

END IF;

END;

.

/
SELECT * FROM Tligne;
