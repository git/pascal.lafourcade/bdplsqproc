SET echo OFF
SET verify OFF
SET feed OFF

DROP TABLE tligne;
CREATE TABLE Tligne(ligne VARCHAR2(200));
DROP TABLE tligne2;
CREATE TABLE Tligne2(ligne VARCHAR2(200));


DECLARE
DnoProduit tProduit.noProduit%TYPE;
Ddesignation tProduit.designation%TYPE;
Dstock tProduit.stock%TYPE;
Dref tFournisseur.ref%TYPE;
DlistFourn VARCHAR(1000);
CURSOR r IS SELECT noProduit,designation,stock FROM tProduit WHERE noProduit NOT IN (SELECT noProduit FROM tProduitFourn);
CURSOR t IS SELECT noProduit,designation,stock FROM tProduit;
CURSOR s IS SELECT ref FROM tProduitFourn WHERE noProduit = DnoProduit;
BEGIN
OPEN r;
INSERT INTO Tligne VALUES('noProduit    designation    stock');
FETCH r INTO DnoProduit,Ddesignation,Dstock;
WHILE r%FOUND
LOOP
	INSERT INTO Tligne VALUES(DnoProduit||' '||Ddesignation||' '||Dstock);
	FETCH r INTO DnoProduit,Ddesignation,Dstock;
END LOOP;
CLOSE r;
OPEN t;
INSERT INTO Tligne2 VALUES('noProduit    designation    stock      reference fournisseur');
FETCH t INTO DnoProduit,Ddesignation,Dstock;
WHILE t%FOUND
LOOP
	DlistFourn :='';
	OPEN s;
	FETCH s INTO Dref;
	WHILE s%FOUND
	LOOP
		DlistFourn := DlistFourn||'   '||Dref;
		FETCH s INTO Dref;
	END LOOP;
	CLOSE s;
	INSERT INTO Tligne2 VALUES(DnoProduit||' '||Ddesignation||' '||Dstock||' 		 '||DlistFourn);
	FETCH t INTO DnoProduit,Ddesignation,Dstock;
END LOOP;
CLOSE t;

END;
.
/



SELECT * FROM Tligne;
SELECT * FROM Tligne2;
SET echo ON
SET verify ON
SET feed ON
