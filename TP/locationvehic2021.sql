DROP TABLE Tlocatretour2021;
DROP TABLE Tlocation2021;
DROP TABLE Tvehicule2021;
DROP TABLE Tremplacement2021;
DROP TABLE Tcategorie2021;
DROP TABLE Tclient2021;

CREATE TABLE Tclient2021
(
noclient  CHAR(4) PRIMARY KEY,
nom VARCHAR2(80)     NOT NULL, 
ville VARCHAR2(80)   NOT NULL,
postal  VARCHAR2(5)  NOT NULL
);


CREATE TABLE Tcategorie2021
(
nocat CHAR(4) PRIMARY KEY,
libelle VARCHAR2(20) NOT NULL
);

CREATE TABLE  Tremplacement2021
(
nocat CHAR(4) references Tcategorie2021,
nocatequi CHAR(4) references Tcategorie2021,
primary key (nocat,nocatequi)
);

CREATE TABLE Tvehicule2021
(
noveh char(5) primary key,
immat char(10) not null,
modele varchar2(30) not null,
couleur varchar2(30) not null,
kilometrage number not null,
nocat CHAR(4) not null references Tcategorie2021
);


CREATE TABLE TLOCATION2021
(
noclient CHAR(4) references Tclient2021 not null,
noveh char(5) primary key references tvehicule2021,
datedeb date not null,
dateretprev date not null,
kmdeb number not null 
);

CREATE TABLE Tlocatretour2021
(
noclient CHAR(4) references Tclient2021 not null,
noveh char(5) not null references tvehicule2021,
datedeb date not null,
kmdeb number not null, 
kmfin number not null,
dateretour date not null,
primary key (noveh,datedeb),
check(dateretour>datedeb),
check(kmfin>kmdeb) 
);


insert into Tclient2021 values ('C001','Dupond','Clermont Fd','63000');
insert into Tclient2021 values ('C002','Martin','Aubiere','63170');
insert into Tclient2021 values ('C003','Dutour','Clermont Fd','63000');

insert into Tcategorie2021 values ('CAT1','legere 1');
insert into Tcategorie2021 values ('CAT2','legere 2');
insert into Tcategorie2021 values ('CAT3','monospace 1');
insert into Tcategorie2021 values ('CAT4','monospace 2');

insert into Tremplacement2021 values ('CAT1','CAT2');
insert into Tremplacement2021 values ('CAT3','CAT4');

insert into Tvehicule2021 values ('VE001','aa-2000-za','clio 3','noire',26500,'CAT1');
insert into Tvehicule2021 values ('VE002','bb-3000-za','308','blanche',20000,'CAT2');
insert into Tvehicule2021 values ('VE003','cc-4000-za','clio 3','noire',5000,'CAT2');
insert into Tvehicule2021 values ('VE004','dd-5000-za','308','grise',1200,'CAT1');
insert into Tvehicule2021 values ('VE005','ff-6000-za','Picasso','noire',6600,'CAT3');


insert into Tlocation2021 values ('C001','VE001',to_date('30-01-2021','DD-MM-YYYY'),to_date('5-02-2021','DD-MM-YYYY'),26500);
insert into Tlocation2021 values ('C001','VE002',to_date('28-01-2021','DD-MM-YYYY'),to_date('7-02-2021','DD-MM-YYYY'),20000);
insert into Tlocation2021 values ('C002','VE003',to_date('29-01-2021','DD-MM-YYYY'),to_date('10-02-2021','DD-MM-YYYY'),5000);

insert into Tlocatretour2021 values ('C001','VE001',to_date('30-12-2020','DD-MM-YYYY'),24500,25000,to_date('5-01-2021','DD-MM-YYYY'));
insert into Tlocatretour2021 values ('C002','VE001',to_date('6-01-2021','DD-MM-YYYY'),25000,26500,to_date('10-01-2021','DD-MM-YYYY'));
insert into Tlocatretour2021 values ('C001','VE002',to_date('30-12-2020','DD-MM-YYYY'),18500,20000,to_date('5-01-2021','DD-MM-YYYY'));
insert into Tlocatretour2021 values ('C003','VE003',to_date('30-11-2020','DD-MM-YYYY'),1500,2500,to_date('5-12-2020','DD-MM-YYYY'));
insert into Tlocatretour2021 values ('C002','VE003',to_date('10-12-2020','DD-MM-YYYY'),2500,5000,to_date('15-12-2020','DD-MM-YYYY'));
