-- Exercice 3
-- 	Afficher le nombre de véhicules par catégorie.

DROP TABLE tligne;
CREATE TABLE tligne (ligne varchar2(300));

set echo off;
set verify off;
set feed off;

declare

dnocat Tvehicule2017.nocat%TYPE; 
dnbveh number;

cursor r is
select  nocat, count(nocat)
from Tvehicule2017
group by nocat
order by nocat;

begin
open r;
fetch r into  dnocat, dnbveh; 

while  r%found
loop
	insert into tligne values (dnocat ||' '||dnbveh); 
	fetch r into dnocat, dnbveh; 
end loop;
close r;
end;
.
/

select * from tligne;

set echo on;
set verify on;
set feed on;
