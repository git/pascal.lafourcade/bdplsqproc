
-- Exercice 2
-- Afficher le numéro du  véhicule loué par un client dont le numéro sera saisi.
-- Cas d’erreur :
-- -	« aucune location pour ce client »
-- -	«  plusieurs locations en cours pour ce client »
--  (nous verrons dans un prochain TP comment traiter ce problème)

-- Test C001, C002, C003 :

-- plusieurs locations pour C001
-- vehicule VE003 est loue par C002
-- aucune location pour C003


DROP TABLE tligne;
CREATE TABLE tligne (ligne varchar2(100));

set echo off;
set verify off;
set feed off;

variable vnoclient char(4)
 
prompt Entrer le numero du  client :
accept vnoclient

declare
dnovehicule  char(5); 

BEGIN

select noveh into dnovehicule
from Tlocation2017
where noclient='&vnoclient';

Insert into tligne values ('vehicule '||dnovehicule||' est loue par '||'&vnoclient');

exception
 when no_data_found then
 insert into tligne values ('aucune location pour '||'&vnoclient');

when too_many_rows then
insert into tligne values ('plusieurs locations pour '||'&vnoclient');

end;
.
/
select * from tligne;

set echo on;
set verify on;
set feed on;


