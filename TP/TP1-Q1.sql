
-- Exercice 1
-- A partir d’un numéro de véhicule saisi au clavier, afficher
-- l’immatriculation et le modèle correspondant ainsi que le nom du
-- client si c’est un véhicule en cours de location.

-- Cas d’erreur : «  n° de véhicule inconnu ».

-- Test VE001
-- aa-2000-za est l immatriculation du vehicule VE001 de modele clio 3 loue par Dup

-- Test VE005
-- ff-6000-za est l immatriculation du vehicule VE005 de modele Picasso

-- Test VE008
-- VE008 vehicule inconnu


DROP TABLE tligne;
CREATE TABLE tligne (ligne varchar2(100));

set echo off;
set verify off;
set feed off;

variable vnumvehicule char(5)
variable toto char(5)

prompt Entrer le numero du vehicule :

accept vnumvehicule

declare

dimmatriculation char(10); 
dmodele varchar2(30);
dmessage  varchar2(100);
dnom  VARCHAR2(80);

begin

dmessage := '&vnumvehicule'||' vehicule inconnu';

select immat, modele into dimmatriculation, dmodele
from Tvehicule2017
where noveh='&vnumvehicule';

dmessage :=  dimmatriculation||' est l immatriculation du vehicule '||'&vnumvehicule'||' de modele '||dmodele;

select nom into dnom
from  Tclient2017, Tlocation2017
where Tlocation2017.noveh='&vnumvehicule' and Tlocation2017.noclient=Tclient2017.noclient; 

Insert into tligne values (dmessage||' loue par '||dnom);


exception
when no_data_found then
     insert into tligne values (dmessage);
end;
.
/
select * from tligne;

set echo on;
set verify on;
set feed on;



