-- Exercice 3
-- On souhaite enregistrer le retour d’un véhicule. Donner la procédure PL/SQL permettant l’enregistrement du retour, pour une date de retour donnée et pour un véhicule dont on saisira le numéro. On saisira également le kilométrage de fin de location.
-- Informations à saisir : numéro du véhicule, date de retour, kilométrage de fin
-- -	Les messages suivants sont à prévoir:
-- -	‘numéro véhicule inconnu
-- -	‘ce véhicule n’est pas en cours de location
-- -	‘la date de retour doit être postérieure à la date de début de location’
-- -	‘le kilométrage de fin doit être supérieur au kilométrage de début
-- -	‘ le retour a bien été enregistré et le kilométrage du véhicule a été mis à jour’

-- TEST
-- VE009
--  numero de vehicule inconnu

-- VE004
-- Vehicule pas en cours de location
-- VE003
-- '03-JAN-20022'
-- 370000

-- PB date de retour inf date debut
-- VE003
-- '03-JAN-2022'
-- 37

-- PB Km de retour inf KM debut
-- VE004
-- '03-JAN-2018'
-- 4000000

-- ORA-00001 : pb de clef non respectee
-- ORA-02290 : check constraint Viole les contraintes d'integrite du modele.

insert into Tlocation2021 values ('C002','VE004',to_date('29-01-2016','DD-MM-YYYY'),to_date('10-02-2021','DD-MM-YYYY'),5000);

DROP TABLE tligne;
CREATE TABLE tligne (ligne varchar2(100));

set echo off;
set verify off;
set feed off;

variable vnovehicule char(5);
variable vdateretour char(10);
variable vkmfin number;

prompt Entrer le numero du vehicule :
accept vnovehicule

prompt Entrer la date de retour :
accept vdateretour

prompt Entrer le kilometrage de fin :
accept vkmfin

declare
dnovehicule char(5);
ddatedebut date;
dkmdeb number(6);
dnoclient char(4);
dmessage varchar2(100);

begin
dmessage:='numero de vehicule inconnu';

select noveh into dnovehicule
from Tvehicule2021
where noveh='&vnovehicule';

dmessage:='Vehicule pas en cours de location';

select noclient into dnoclient
from Tlocation2021
where noveh='&vnovehicule';

select datedeb, kmdeb, noclient into ddatedebut, dkmdeb, dnoclient
from Tlocation2021
where noveh='&vnovehicule';

if (ddatedebut >= TO_DATE(&vdateretour,'DD-MM-YYYY'))
 then Insert into tligne values ('PB date de retour inf date debut');

else
if (dkmdeb >= &vkmfin)
 then Insert into tligne values ('PB Km de retour inf KM debut');
else

Insert into Tlocatretour2021 values (dnoclient,'&vnovehicule',ddatedebut,dkmdeb,&vkmfin,to_date(&vdateretour,'DD-MM-YYYY'));
-- Insert into Tlocatretour2021 values (dnoclient,'&vnovehicule',ddatedebut,dkmdeb,&vkmfin,to_date('15-12-2019','DD-MM-YYYY'));

Insert into tligne values ('Retour bien effectue ');

Delete Tlocation2021 WHERE  noveh='&vnovehicule';

Insert into tligne values ('Effacement location en cours ');

end if;
end if;

exception 
  when no_data_found
  then insert into tligne values (dmessage);

end;
.
/

select * from tligne;

set echo on;
set verify on;
set feed on;
